check() {
    #kumpulan variabel local
    local lengthPassword=${#password}
    local locFolder=/home/mfuadsalim/SISOP/MODUL1/Soal1
    local locUsers=$locFolder/users
    
    
    #check ada direktori atau tidak 
    if [[ ! -d "$locUsers" ]]
    then
        mkdir $locUsers
    fi

    if grep -q -w "$username" "$locUsers/user.txt"
    then
    
    exits="User Already Exist"
    
    echo $exits
    echo $calendar $time REGISTER:ERROR $exits >> $locFolder/log.txt
    
    elif [[ $password == $username ]]
    then
    echo "Your Password Cannot be the Same as Username"

    elif [[ $lengthPassword -lt 8 ]]
    then
    echo "Password Must be More than 8 Character"

    elif [[ "$password" != *[[:upper:]]* || "$password" != *[[:lower:]]* || "$password" != *[0-9]* ]]
    then 
    echo "Password Must Contain Uppercase Letters, Lowercase Letters, and Alphanumeric"

    else
    
    echo "Register Successfully!"
    echo $calendar $time REGISTER:INFO User $username Registered Succesfully! >> $locFolder/log.txt
    echo $username $password >> $locUsers/users.txt
    
    fi

}



calendar=$(date +%D)
time=$(date +%T)

printf "Enter Your Username : "
read username

printf "Enter Your Password : "
read -s password

if [[ $(id -u) -ne 0 ]]
then 
echo -e "\nYou don't have permission as a root, please run as root!"
exit 1
else
    check
    fi
