# Soal Shift Sisop Modul 1 C05 2022


## Anggota Kelompok ##

NRP | NAMA
------------- | -------------
5025201057    | Muhammad Fuad Salim
5025201112    | Naufal Ariq Putra Yosyam
5025201139    | Nabila Zakiyah Khansa' Machrus

# SOAL 1
Pada Soal 1 diminta untuk membuat 2 buah script yaitu register.sh dan main.sh. Pada script register.sh digunakan untuk register user dan password dengan ketentuan yang ada di soal. Kemudian untuk main.sh sendiri digunakan untuk login user dan menjalankan beberapa command berupa [dl N atau att].

## register.sh
Pada script ini akan dilakukan input username dan juga password namun ketika password akan di hide ketika di input. Kemudian pada script register.sh ini terdapat suatu fungsi check. Di fungsi check ini sendiri akan dilakukan pengecekan terhadap input yang dimasukkan mulai dari username kemudian password.

## main.sh
Di script main.sh ini digunakan untuk login dengan username dan password yang telah di register pada script register.sh. Selain itu kita dapat menjalankan dua command yaitu dl N dan atau att. dl N merupakan command untuk mendownload gambar dari website  https://loremflickr.com/320/240 sebanyak N, setelah itu seluruh gambar yang telah di download akan di zip dengan format nama yang sesuai dengan yang ada di soal. Lalu untuk att sendiri merupakan command untuk menghitung jumlah login user yang sedang login baik yang berhasil maupun yang gagal. Di sini kami menggunakan beberapa fungsi seperti check, login, fun_att, fun_dl, fun_start_dl, dan fun_unzip. 

# SOAL 2
Soal ini bertujuan untuk untuk membuat suatu program yang dapat menganalisis forensik melalui script awk. Dengan studi kasus website (https://daffa.info) yang dihack, Kita diharapkan mampu menunjukkan rata-rata requests per jam, menampilkan IP dengan jumlah requests terbanya, menampilkan banyak requests yang menggunakan user-agent curl, dan menampilkan IP yang mengakses pada pukul 2 pagi.

## membuat folder
Untuk membuat folder forensic_log_website_daffainfo_log digunakan command mkdir namafolder.

## Rata-rata request per jam
Untuk menghitung rata-rata request per jam, langkah pertama yang dilakukan yaitu mencari jumlah requests yang masuk dengan menggunakan awk dimana setiap menemukan kata Jan maka variabel n akan bertambah. Setelah itu jumlah variabel n akan dibagi dengan 12 dan akan menghasilkan jumlah rata-rata request per jam. Hasil dari proses tersebut kemudian akan dimasukkan ke dalam file ratarata.txt.

## IP dengan requests terbanyak
Untuk menemukan Ip dengan requests terbanyak, Kami menggunakan looping pada AWK yang menggunakan array a yang dimana setiap i menotasikan setiap nilai dalam array a. Setelah itu dilakukan command sort -k 10rn | head -1 untuk mengurutkan ip berdasarkan jumlah request terbanyak dan diprint hanya baris pertama yang merupakan ip dengan request terbanyak. Hasil dari proses tersebut kemudian dimasukkan ke dalam file result.txt.

## Jumlah request yang menggunakan user-agent curl
Untuk menghitung jumlahnya, Kami menggunakan AWK dimana setiap ketemu curl makan variabel n bertambah. Hasil dari proses tersebut kemudian dimasukkan ke dalam file result.txt. 

## Ip yang mengakses jam 2 pagi
Untuk mendaptkan Ip yang mengakses jam 2 pagi, Kami menggunakan Awk dimana setiap ketemu jam 02 maka IP pada baris tersebut akan dicetak. Hasil dari proses tersebut kemudian dimasukkan ke dalam file result.txt.

## kendala dan error yang ditemukan saat mengerjakan praktikum soal ke 2
Saat pembuatan folder pertama kali terdapat kesalahan dalam penulisan nama folder sehingga ketika melakukan save atau insert hasil awk ke file dalam folder tidak dapat dilakukan. Kami juga menemukan beberapa kali syntax error yang ketika dihapus dan ditulis kembali persis seperti syntax awal malah dapat dirun.

## SOAL 3

REVISI
terjadi error saat menggabungkan perintah di tiap command sehingga tidak bisa concat

3. Membuat program monitoring resource. Dengan memakai command `free -m`dan command `du -sh <target_path>`. Mencatat semua metrics dan catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/. 

A. Metrics yang dicatat dimasukkan pada file Log dengan format {YmdHms}, sebagai waktu dijalankan
```bash 
#!/bin/bash
loc_dir=/home/$(whoami)/log

free -m | awk '
  BEGIN {
    printf "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n"
  }
  FNR==2 {
    printf "%s,%s,%s,%s,%s,%s", $2,$3,$4,$5,$6,$7
  }
  FNR==3 {
    printf "%s,%s,%s", $2,$3,$4
  }
' >> $loc_dir/metrics_$(date +%Y%m%d%H%M%S).log

du -sh /home/$(whoami) | awk '
  { printf ",%s,%s", $2, $1 }
' >> $loc_dir/metrics_$(date +%Y%m%d%H%M%S).log | chmod 400 $loc_dir/metrics_$(date +%Y%m%d%H%M%S).log
```

B. Metrics berjalan otomatis setiap menit
```bash 
if [[ ! -d $loc_dir ]]; then
  mkdir $loc_dir
  (crontab -l | grep --invert-match --fixed-strings "$(pwd)/minute_log.sh"; echo "* * * * * $(pwd)/minute_log.sh") | crontab -
fi

(crontab -l | grep --invert-match --fixed-strings "$(pwd)/minute_log.sh"; echo "* * * * * $(pwd)/minute_log.sh") | crontab -
```

D. Agar file hanya bisa dibaca oleh user tertentu
```bash 
' >> $loc_dir/metrics_$(date +%Y%m%d%H%M%S).log | chmod 400 $loc_dir/metrics_$(date +%Y%m%d%H%M%S).log
```
#KENDALA# 

Terminal tidak bisa memberi ijin akses ke file metrics

no 3 C belum selesai

