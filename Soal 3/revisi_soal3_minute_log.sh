#!/bin/bash
loc_dir=/home/$(whoami)/log

free -m | awk '
  BEGIN {
    printf "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n"
  }
  FNR==2 {
    printf "%s,%s,%s,%s,%s,%s", $2,$3,$4,$5,$6,$7
  }
  FNR==3 {
    printf "%s,%s,%s", $2,$3,$4
  }
' >> $loc_dir/metrics_$(date +%Y%m%d%H%M%S).log

du -sh /home/$(whoami) | awk '
  { printf ",%s,%s", $2, $1 }
' >> $loc_dir/metrics_$(date +%Y%m%d%H%M%S).log | chmod 400 $loc_dir/metrics_$(date +%Y%m%d%H%M%S).log

if [[ ! -d $loc_dir ]]; then
  mkdir $loc_dir
  (crontab -l | grep --invert-match --fixed-strings "$(pwd)/minute_log.sh"; echo "* * * * * $(pwd)/minute_log.sh") | crontab -
fi

(crontab -l | grep --invert-match --fixed-strings "$(pwd)/minute_log.sh"; echo "* * * * * $(pwd)/minute_log.sh") | crontab -
